# Retrieve Ameren Data #

### Install R ###
Download and install the latest release of R here:
https://cran.r-project.org/bin/macosx/

### Install Packages ###
Once installed, run R by typing `R` at the command line.

Install required packages by running each of these commands at the prompt:

`install.packages("arrow")`

`install.packages("aws.s3", repos = "https://cloud.R-project.org")`

Close out of R by pressing Ctrl+D

### Get AWS access keys ###
Once logged into the AWS console, click the "Command line or programmatic access" link and your access key window will open:
![alt text](docs/console.png "AWS Console")

![alt text](docs/keys.png "AWS Access Keys")

### Run script ###
To run the script, navigate to your project directory and run:

`Rscript main.R`

At the prompts, copy in the correct API keys that you received from the AWS console (the most simple way is by clicking the "copy" link in "Option 3" in the image above.)

**Note:** The access keys will expire after 4 hours.  After 4 hours, you will need to retrieve new keys from the AWS console.
